$(document).ready(function () {
  var grandpaStep = $('.steps-horizon>.steps-item');
  var parentStep = '.steps-item';
  var indexStep = grandpaStep.index(parentStep);
  var grandpaContent = $('.steps-content-wrap>.steps-content');
  var parentContent = $('.steps-content');
  var indexContent = grandpaContent.index(parentContent);
  var fileUploadExam = $('#fileUploadExam');
  var uploadAreaExam = $('.upload-area-exam');
  var uploadAreaAnswer = $('.upload-area-answer');
  var fileUploadAnswer = $('#fileUploadAnswer');
  var contentSteps = $('.js-content-steps');
  var contentFinish = $('.js-content-finish');
  var inputSchool = $('.js-input-school');
  var inputYear = $('.js-input-year');

  var rulesStep1 = {
    lblErrorExtension: $('.error-msg-extension'),
    lblErrorSize: $('.error-msg-size'),
    btnNext: $('.js-btn-next-step1'),
    maxSize: 10
  };

  var rulesStep2 = {
    lblErrorExtension: $('.error-msg-extension-step2'),
    lblErrorSize: $('.error-msg-size-step2'),
    btnNext: $('.js-btn-next-step2'),
    maxSize: 20
  };

  $('html').scrollTop(0);

  preventRedirect();

  // go to next step
  $('body').on('click', '.btn-next', function (e) {
    e.preventDefault();
    $('html').scrollTop(0);


    indexStep = indexStep + 1;
    indexContent = indexContent + 1;
    grandpaStep.eq(indexStep).addClass('steps-item--compeleted');
    parentContent.removeClass('steps-content--active');
    grandpaContent.eq(indexContent).addClass('steps-content--active');
  });

  // go to previous step
  $('body').on('click', '.btn-previous', function (e) {
    e.preventDefault();
    grandpaStep.eq(indexStep).removeClass('steps-item--compeleted');
    indexStep = indexStep - 1;
    parentContent.removeClass('steps-content--active');
    indexContent = indexContent - 1;
    grandpaContent.eq(indexContent).addClass('steps-content--active');
  });

  // Step 1
  rulesStep1.btnNext.prop('disabled', true).css('cursor', 'not-allowed');
  dragDropFile(rulesStep1, uploadAreaExam);

  $('body').on('click', '#fileUploadExam', function () {
    fileUploadExam.change(function () {
      if (fileUploadExam[0].files[0]) {
        fileUploadExamName = fileUploadExam[0].files[0];
      }
      validateFileUpload(rulesStep1, fileUploadExamName);
    });
  });

  // Step 2
  rulesStep2.btnNext.prop('disabled', true).css('cursor', 'not-allowed');
  dragDropFile(rulesStep2, uploadAreaAnswer);
  $('body').on('click', '#fileUploadAnswer', function () {
    fileUploadAnswer.change(function () {
      if (fileUploadAnswer[0].files[0]) {
        fileUploadAnswerName = fileUploadAnswer[0].files[0];
      }
      validateFileUpload(rulesStep2, fileUploadAnswerName);
    });
  });

  // step 3
  $('.js-btn-next-step3')
    .prop('disabled', true)
    .css('cursor', 'not-allowed');
  inputSchool.on('keyup focusout', function () {
    validateRequired(inputSchool);
    validateStep3(inputSchool, inputYear);
  });

  inputYear.on('keyup focusout', function () {
    validateRequired(inputYear);
    validateStep3(inputSchool, inputYear);
  });
  // step 4
  var editor;
  $('body').on('click', '.js-btn-update', function () {
    editor = com.wiris.jsEditor.JsEditor.newInstance({ language: 'en' });
    editor.insertInto(document.getElementById('editorContainer'));
  });
  // step finish

  $('body').on('click', '.js-btn-compeleted', function (e) {
    e.preventDefault();
    contentSteps.addClass('js-display--none');
    contentFinish.removeClass('js-display--none');
  });
});
