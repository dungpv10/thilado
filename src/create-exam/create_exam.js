$(document).ready(function() {
  var body = $('body');
  function activeTab(element) {
    $('.tabs-bar__item').removeClass('tabs-bar__item--active');
    $(element).addClass('tabs-bar__item--active');
    var id = $(element).attr('href');
    console.log(id);

    $('.questions').hide();
    $(id).show();
  }
  activeTab($('.tabs-bar__item--active'));

  body.on('click', '.tabs-bar__item', function() {
    activeTab(this);
    return false;
  });
});
