var allowedFiles = ['.xls', '.xlsx'];
var regexFileUpload = new RegExp(
  '([a-zA-Z0-9s_\\.-:])+(' + allowedFiles.join('|') + ')$'
);

function validateFileUpload(rules, fileUpload) {
  var fileUploadName = fileUpload.name;
  var fileUploadSize = fileUpload.size;

  rules.lblErrorSize.addClass('js-display--none');
  rules.lblErrorExtension.removeClass('js-display--none');
  if (fileUploadSize < rules.maxSize * 1024 * 1024) {
    if (!regexFileUpload.test(fileUploadName.toLowerCase())) {
      rules.lblErrorExtension.html(
        'Chỉ tải file có định dạng:  ' + allowedFiles.join(', ') + '.'
      );
      if (rules.lblErrorExtension.hasClass('js-file-name')) {
        rules.lblErrorExtension.removeClass('js-file-name');
      }
      rules.btnNext.prop('disabled', true).css('cursor', 'not-allowed');
      return false;
    } else {
      rules.lblErrorExtension.html(fileUploadName).addClass('js-file-name');
    }
    rules.lblErrorExtension.removeClass('js-display--none');
  } else {
    rules.lblErrorExtension.addClass('js-display--none');
    rules.lblErrorSize.html(
      'Chỉ tải file có dung lượng nhỏ hơn ' + rules.maxSize + 'Mb.'
    );
    rules.btnNext.prop('disabled', true).css('cursor', 'not-allowed');
  }

  if (
    rules.lblErrorExtension.hasClass('js-file-name') &&
    rules.lblErrorSize.hasClass('js-display--none')
  ) {
    rules.btnNext.prop('disabled', false).css('cursor', 'pointer');
  }
}

function validateRequired(element) {
  var label = $("label[for='" + element.attr('id') + "']");
  if (element.val() == '') {
    element.addClass('js-input-error text-danger');
    label.addClass('text-danger');
    return false;
  } else {
    element.removeClass('js-input-error text-danger');
    label.removeClass('text-danger');
    return true;
  }
}

function validateStep3(inputSchool, inputYear) {
  if (validateRequired(inputSchool) && validateRequired(inputYear)) {
    $('.js-btn-next-step3')
      .prop('disabled', false)
      .css('cursor', 'pointer');
  } else {
    $('.js-btn-next-step3')
      .prop('disabled', true)
      .css('cursor', 'not-allowed');
  }
}
