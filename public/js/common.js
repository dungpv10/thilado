$(document).ready(function () {
  var sidebarMobile = $('.sidebar--mobile');
  var toggleMenuMobile = $('.js-toggle-mobile-expend');

  $('html').scrollTop(0);

  $('body').on('click', '.toggle', function () {
    $('#sidebar').toggleClass('active');
  });
  if ($(window).width() < 576) {
    $('body').on('click', '.js-toggle-mobile-expend', function () {
      sidebarMobile
        .addClass('active')
        .css('display', 'block')
        .addClass('sidebar--expand');
    });
    $('body').on('click', '.js-toggle-mobile', function () {
      sidebarMobile
        .removeClass('sidebar--expand')
        .css('display', 'none')
        .removeClass('active');
    });
    $('.list-item li>a').on('click', function (event) {
      event.preventDefault();
    });
  } else {
    sidebarMobile.css('display', 'block');
  }
});
