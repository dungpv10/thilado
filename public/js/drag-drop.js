function preventRedirect() {
  $('html').on('dragover', function(e) {
    e.preventDefault();
    e.stopPropagation();
    $('h1').text('Drag here');
  });
  $('html').on('drop', function(e) {
    e.preventDefault();
    e.stopPropagation();
  });
}

function dragDropFile(rules, uploadArea) {
  // Drag enter
  uploadArea.on('dragenter', function(e) {
    e.stopPropagation();
    e.preventDefault();
    $('h1').text('Drop');
  });

  // Drag over
  uploadArea.on('dragover', function(e) {
    e.stopPropagation();
    e.preventDefault();
    $('h1').text('Drop');
  });

  // Drop
  uploadArea.on('drop', function(e) {
    e.stopPropagation();
    e.preventDefault();
    var file = e.originalEvent.dataTransfer.files[0];
    fileUploadExamName = file;
    var fd = new FormData();
    fd.append('file', file);
    validateFileUpload(rules, fileUploadExamName);
  });
}
